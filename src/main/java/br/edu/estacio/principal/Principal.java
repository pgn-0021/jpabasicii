package br.edu.estacio.principal;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.edu.estacio.model.Pessoa;

public class Principal {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("Compras");
		EntityManager em = emf.createEntityManager();
		
		//Coloque o seu código aqui!!
		Pessoa maria = new Pessoa();
		maria.setNome("Maria");
		maria.setSobrenome("Joana");
		
		Pessoa joao = new Pessoa();
		joao.setNome("João");
		joao.setSobrenome("Henrique");
		
		em.getTransaction().begin();
		em.persist(maria);
		em.persist(joao);
		em.getTransaction().commit();
				
		em.close();
		emf.close();
	}

}
